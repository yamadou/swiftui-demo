//
//  ContentView.swift
//  LoanApp
//
//  Created by Yamadou Traore on 29/10/19.
//  Copyright © 2019 Yamadou Traore All rights reserved.
//

import SwiftUI

struct ContentView: View {

  var body: some View {

    VStack(spacing: 60) {
      // MARK: - Top View
      VStack {
        HStack(alignment: .top, spacing: 30) {
          InfoContainer()
          CardContainer()
        }.padding(.top, -40)
      }

      // MARK: - Bottom View
      VStack(alignment: .leading, spacing: 30) { // Bottom View
        InfosView(title: "Load requested", value: "35.400", textSize: 55, theme: .dark)
        InfosView(title: "Interest", value: "2.3%", textSize: 55, theme: .dark)

        HStack {
          Button(action: buttonTapped) {
            Image("filter-icon")
              .renderingMode(.original)
              .frame(width: 80, height: 60)
              .overlay(RoundedRectangle(cornerRadius: 20)
                .stroke(Color(UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)), lineWidth: 2))
          }
          Button(action: buttonTapped) {
            Text("Send Request")
              .fontWeight(.heavy)
              .font(.custom("Gilroy-ExtraBold", size: 14))
              .foregroundColor(.white)
              .frame(width: 225, height: 60)
              .background(Color(#colorLiteral(red: 1, green: 0.5137254902, blue: 0.3725490196, alpha: 1)))
              .cornerRadius(20)
          }
          Spacer()
        }

      }
      .frame(height: 300)
      .padding(.all, 30)
      .background(Color(UIColor(red: 0.02, green: 0.07, blue: 0.16, alpha: 1.00)))
      .clipShape(RoundedTopShape(cornerRadius: 50, style: .circular))
      .padding(.bottom, -170)
    }.edgesIgnoringSafeArea(.all)
  }

  func buttonTapped() {
    print("button tapped")
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
      .previewDevice(PreviewDevice(rawValue: "iPhone X"))
  }
}

struct BackButton: View {
  var body: some View {
    Button(action: { print("Hello World") }) {
      Image("icon-back")
        .frame(width: 35, height: 50)
        .overlay(RoundedRectangle(cornerRadius: 10)
          .stroke(Color(hue: 219/255, saturation: 79/255, brightness: 9/255, opacity: 0.1), lineWidth: 2))
    }
  }
}

struct InfosView: View {

  enum Theme {
    case light
    case dark
  }

  struct Colors {
    static let titleColorLight: Color = Color(UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 0.2))
    static let valueColorLight: Color = Color(UIColor(red: 0.02, green: 0.07, blue: 0.16, alpha: 1.00))
    static let titleColorDark: Color = Color(UIColor(red: 1, green: 1, blue: 1, alpha: 0.7))
    static let valueColorDark: Color = Color(UIColor(red: 1, green: 1, blue: 1, alpha: 1))
  }

  var title: String
  var value: String
  var textSize: CGFloat
  var theme: Theme = .light

  var body: some View {
    VStack(alignment: .leading) {
      Text(title)
        .fontWeight(.heavy)
        .font(.custom("Gilroy-ExtraBold", size: 14))
        .foregroundColor(theme == .light ? Colors.titleColorLight : Colors.titleColorDark)
      Text(value)
        .font(.custom("Gilroy-ExtraBold", size: textSize))
        .fontWeight(.bold)
        .foregroundColor(theme == .light ? Colors.valueColorLight : Colors.valueColorDark)
    }
  }
}

struct CardView: View {
  var body: some View {
    ZStack {
      VStack(alignment: .leading) {
        Image("mastercard").padding()
        Spacer()
      }
      .padding(.leading, -100)
    }
    .frame(width: 200, height: 330)
    .background(Color(UIColor(red: 0.93, green: 0.94, blue: 0.95, alpha: 1.00)))
    .cornerRadius(20)
    .shadow(color: Color(UIColor(red: 0, green: 0, blue: 0, alpha: 0.2)), radius: 10, x: 20, y: 5)
  }
}

struct CardContainer: View {
  var body: some View {
    ZStack {
      CardView()
        .offset(x: 50, y: 40)

      CardView()
        .offset(x: 25, y: 20)

      CardView()
    }
    .padding(.trailing, -80)
  }
}

struct InfoContainer: View {

  var body: some View {
    VStack(alignment: .leading, spacing: 30) {

      BackButton()

      InfosView(title: "Selected Card", value: "*6996", textSize: 55, theme: .light)

      VStack(alignment: .leading, spacing: 20) {
        InfosView(title: "Holder", value: "Jameson Lancaster", textSize: 18)
        HStack {
          InfosView(title: "Expire Date", value: "19.24", textSize: 18)
          InfosView(title: "CVV", value: "939", textSize: 18)
        }
      }

      Button(action: { print("button clicked") }) {
        HStack(alignment: .firstTextBaseline, spacing: 4) {
          Text("edit card")
            .font(.custom("Gilroy-ExtraBold", size: 15))
            .foregroundColor(Color(UIColor(red: 1.00, green: 0.51, blue: 0.37, alpha: 1.00)))
          Image("icon-chevron")
        }
      }

    }
  }
}


struct RoundedTopShape: Shape {

  var cornerRadius: CGFloat
  var style: RoundedCornerStyle

  func path(in rect: CGRect) -> Path {
    let path = UIBezierPath(roundedRect: rect, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
    return Path(path.cgPath)
  }
}
